package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(@NotNull MessageListener listener) throws JMSException;

}
