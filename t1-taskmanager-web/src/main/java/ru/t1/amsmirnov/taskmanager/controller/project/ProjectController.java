package ru.t1.amsmirnov.taskmanager.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.model.ProjectWeb;
import ru.t1.amsmirnov.taskmanager.repository.ProjectWebRepository;

@Controller
public class ProjectController {

    @Autowired
    private ProjectWebRepository projectRepository;

    @PostMapping("/project/create")
    public String create() {
        projectRepository.save(new ProjectWeb("Project_" + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @PostMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final ProjectWeb project = projectRepository.findById(id);
        if (project == null)
            return new ModelAndView("redirect:/projects");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping("/project/edit/{id}")
    public ModelAndView edit(@ModelAttribute("project") ProjectWeb project, BindingResult result){
        projectRepository.save(project);
        return new ModelAndView("redirect:/projects");
    }

}
