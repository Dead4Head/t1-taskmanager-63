package ru.t1.amsmirnov.taskmanager.controller.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.repository.ProjectWebRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskWebRepository;

import java.util.HashMap;
import java.util.Map;

@Controller
public class TasksController {

    @Autowired
    private TaskWebRepository taskRepository;

    @Autowired
    private ProjectWebRepository projectRepository;

    @GetMapping("/tasks")
    public ModelAndView tasks() {
        final Map<String, ?> model = new HashMap<String, Object>() {{
            put("tasks", taskRepository.findAll());
            put("projectRepository", projectRepository);
        }};

        return new ModelAndView("task-list", model);
    }

}
