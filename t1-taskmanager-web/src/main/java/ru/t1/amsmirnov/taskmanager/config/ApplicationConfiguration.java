package ru.t1.amsmirnov.taskmanager.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.amsmirnov.taskmanager")
public class ApplicationConfiguration {

}
