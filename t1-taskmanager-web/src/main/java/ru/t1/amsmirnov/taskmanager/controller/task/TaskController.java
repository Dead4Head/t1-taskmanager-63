package ru.t1.amsmirnov.taskmanager.controller.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.model.TaskWeb;
import ru.t1.amsmirnov.taskmanager.repository.ProjectWebRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskWebRepository;

@Controller
public class TaskController {

    @Autowired
    private TaskWebRepository taskRepository;

    @Autowired
    private ProjectWebRepository projectRepository;

    @PostMapping("/task/create")
    public String create() {
        taskRepository.save(new TaskWeb("Task_" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @PostMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final TaskWeb task = taskRepository.findById(id);
        if (task == null)
            return new ModelAndView("redirect:/tasks");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

    @PostMapping("/task/edit/{id}")
    public ModelAndView edit(@ModelAttribute("task") TaskWeb task, BindingResult result) {
        if ("".equals(task.getProjectId())) task.setProjectId(null);
        taskRepository.save(task);
        return new ModelAndView("redirect:/tasks");
    }

}
