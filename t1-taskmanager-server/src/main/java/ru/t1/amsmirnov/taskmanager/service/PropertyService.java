package ru.t1.amsmirnov.taskmanager.service;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;

@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private String applicationVersionKey = "buildNumber";

    @NotNull
    private String authorEmailKey = "email";

    @NotNull
    private String authorNameKey = "developer";

    @NotNull
    @Value("#{environment['password.iteration']}")
    private String passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['server.port']}")
    public String serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['session.key']}")
    public String session;

    @NotNull
    @Value("#{environment['session.timeout']}")
    public String sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    public String dbUser;

    @NotNull
    @Value("#{environment['database.password']}")
    public String dbPassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String dbUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String dbDriver;

    @NotNull
    @Value("#{environment['database.sql_dialect']}")
    private String dbDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dbHbm2DdlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String dbShowSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String dbFormatSql;

    @NotNull
    @Value("#{environment['database.comment_sql']}")
    private String dbCommentSql;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    private String dbSecondLvlCash;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String dbFactoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cache']}")
    private String dbUseQueryCash;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String dbUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String dbRegionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String dbHazelConfig;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(applicationVersionKey);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(authorNameKey);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(authorEmailKey);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return Integer.parseInt(passwordIteration);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return passwordSecret;
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        return Integer.parseInt(serverPort);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return serverHost;
    }

    @NotNull
    @Override
    public String getSession() {
        return session;
    }

    @Override
    public int getSessionTimeout() {
        return Integer.parseInt(sessionTimeout);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return dbUser;
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return dbPassword;
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return dbUrl;
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return dbDriver;
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return dbDialect;
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return dbHbm2DdlAuto;
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return dbShowSql;
    }

    @NotNull
    @Override
    public String getDatabaseFormatSql() {
        return dbFormatSql;
    }

    @NotNull
    @Override
    public String getDatabaseCommentsSql() {
        return dbCommentSql;
    }

    @NotNull
    @Override
    public String getDataBaseSecondLvlCache() {
        return dbSecondLvlCash;
    }

    @NotNull
    @Override
    public String getDataBaseFactoryClass() {
        return dbFactoryClass;
    }

    @NotNull
    @Override
    public String getDataBaseUseQueryCache() {
        return dbUseQueryCash;
    }

    @NotNull
    @Override
    public String getDataBaseUseMinPuts() {
        return dbUseMinimalPuts;
    }

    @NotNull
    @Override
    public String getDataBaseRegionPrefix() {
        return dbRegionPrefix;
    }

    @NotNull
    @Override
    public String getDataBaseHazelConfig() {
        return dbHazelConfig;
    }

}
