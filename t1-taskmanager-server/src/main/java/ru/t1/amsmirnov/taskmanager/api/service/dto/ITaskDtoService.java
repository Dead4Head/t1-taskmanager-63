package ru.t1.amsmirnov.taskmanager.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.util.List;

public interface ITaskDtoService extends IAbstractUserOwnedDtoService<TaskDTO> {

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    TaskDTO changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

}
