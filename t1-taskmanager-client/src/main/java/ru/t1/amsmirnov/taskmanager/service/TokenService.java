package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.service.ITokenService;

@Service
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

    public TokenService() {}

    @Nullable
    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void setToken(@Nullable final String token) {
        this.token = token;
    }

}
