package ru.t1.amsmirnov.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskShowByIdRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskShowByIdResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show task by ID.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken(), id);
        @NotNull final TaskShowByIdResponse response = taskEndpoint.findOneTaskById(request);
        if (!response.isSuccess() || response.getTask() == null)
            throw new CommandException(response.getMessage());
        showTask(response.getTask());
    }

}
