package ru.t1.amsmirnov.taskmanager.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.listener.IClientListener;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;

@Component
public abstract class AbstractListener implements IClientListener {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
