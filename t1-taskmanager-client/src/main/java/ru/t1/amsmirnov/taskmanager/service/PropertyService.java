package ru.t1.amsmirnov.taskmanager.service;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;

@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private final String applicationVersionKey = "buildNumber";

    @NotNull
    private final String authorEmailKey = "email";

    @NotNull
    private final String authorNameKey = "developer";

    @NotNull
    @Value("#{environment['server.port']}")
    public String serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(applicationVersionKey);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(authorNameKey);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(authorEmailKey);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return Integer.parseInt(serverPort);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return serverHost;
    }

}
