package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataBase64LoadRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataBase64LoadResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load data from base46 dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataBase64LoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        @NotNull final DataBase64LoadResponse response = domainEndpoint.loadDataBase64(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
