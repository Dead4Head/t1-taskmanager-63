package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class UserLoginResponse extends AbstractResultResponse {

    @NotNull
    private String token;

    public UserLoginResponse() {
    }

    public UserLoginResponse(@NotNull String token) {
        this.token = token;
    }

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(@NotNull final String token) {
        this.token = token;
    }

}