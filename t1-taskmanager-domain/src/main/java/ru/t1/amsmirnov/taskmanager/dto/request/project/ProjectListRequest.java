package ru.t1.amsmirnov.taskmanager.dto.request.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;

public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

    public ProjectListRequest() {
    }

    public ProjectListRequest(
            @Nullable final String token,
            @Nullable final ProjectSort sort
    ) {
        super(token);
        setSort(sort);
    }

    @Nullable
    public ProjectSort getSort() {
        return sort;
    }

    public void setSort(@Nullable final ProjectSort sort) {
        this.sort = sort;
    }

}
