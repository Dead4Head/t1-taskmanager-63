package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataBase64SaveResponse extends AbstractResultResponse {

    public DataBase64SaveResponse() {
    }

    public DataBase64SaveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
