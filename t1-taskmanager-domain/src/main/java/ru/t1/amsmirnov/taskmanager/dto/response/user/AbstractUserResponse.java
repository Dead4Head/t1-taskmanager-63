package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse() {
    }

    protected AbstractUserResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    protected AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public UserDTO getUser() {
        return user;
    }

    public void setUser(@Nullable final UserDTO user) {
        this.user = user;
    }

}